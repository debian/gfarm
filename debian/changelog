gfarm (2.7.20+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 25 Mar 2022 18:31:13 +1100

gfarm (2.7.19+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Install missing files (Thanks, Osamu Tatebe).
  * Updated "watch" file.
  * (Build-)Depends += "pkgconf | pkg-config".
  * -dev package to provide "gfarm.pc".
  * gfmd: added "ruby" to Recommends.
  * No longer build with "--as-needed".
  * Rules-Requires-Root: no.
  * Standards-Version: 4.6.0.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 24 Mar 2022 14:42:44 +1100

gfarm (2.7.17+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 07 Oct 2020 02:45:27 +1100

gfarm (2.7.16+dfsg-1) unstable; urgency=medium

  * New upstream release
  * DH to version 13
  * Standards-Version: 4.5.0
  * Watch file to track GitHub releases

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 26 May 2020 15:54:14 +1000

gfarm (2.7.15+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: 4.4.1.
  * postrm: run cleanup after "#DEBHELPER#" to leave no unowned files after
    purge (Closes: #788253).
    Thanks, Felipe Sateler.

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 18 Nov 2019 16:35:53 +1100

gfarm (2.7.11+dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No change (re-upload to get the source back).

 -- Sebastian Andrzej Siewior <sebastian@breakpoint.cc>  Mon, 25 Feb 2019 20:10:20 +0100

gfarm (2.7.11+dfsg-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol

  [ Dmitry Smirnov ]
  * New upstream release.
  * Standards-Version: 4.3.0.
  * Debhelper & compat to version 11.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 02 Feb 2019 15:11:29 +1100

gfarm (2.6.15+dfsg-1) unstable; urgency=medium

  * New upstream release [December 2016].
    + fixed FTBFS with openssl 1.1.0 (Closes: #828310).
  * Install "config-gfarm.common" (Closes: #845006).

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 04 Dec 2016 13:08:17 +1100

gfarm (2.6.13+dfsg-1) unstable; urgency=medium

  * New upstream release [September 2016].

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 08 Oct 2016 13:40:20 +1100

gfarm (2.6.12+dfsg-1) unstable; urgency=medium

  * New upstream release [August 2016].
  * Removed obsolete "alpha-avoid-signal-name-conflict.patch".

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 28 Aug 2016 11:46:47 +1000

gfarm (2.6.11+dfsg-2) unstable; urgency=medium

  * New patch to fix FTBFS on Alpha (Closes: #827021).
    Thanks, Michael Cree.

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 13 Jun 2016 14:12:09 +1000

gfarm (2.6.11+dfsg-1) unstable; urgency=medium

  * New upstream release [June 2016].
  * Standards-Version: 3.9.8.
  * Vcs-Git URL to HTTPS.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 05 Jun 2016 23:15:55 +1000

gfarm (2.6.10+dfsg-2) unstable; urgency=medium

  * Updated missing symbols/i386.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 05 May 2016 14:58:22 +1000

gfarm (2.6.10+dfsg-1) unstable; urgency=medium

  * New upstream release [April 2016].
  * symbols: Introduced "Build-Depends-Package" field.

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 25 Apr 2016 18:50:50 +1000

gfarm (2.6.9+dfsg-1) unstable; urgency=medium

  * New upstream release [April 2016].
  * Removed obsolete "build-arm.patch".

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 04 Apr 2016 05:01:37 +1000

gfarm (2.6.8+dfsg-2) unstable; urgency=medium

  * Fixed FTBFS when built with "dpkg-buildpackage -A" (Closes: #806030).
  * New patch to fix FTBFS on [armel,armhf].
  * Standards-Version: 3.9.7.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 26 Mar 2016 17:13:38 +1100

gfarm (2.6.8+dfsg-1) unstable; urgency=medium

  * New upstream release [January 2016].

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 16 Jan 2016 21:49:41 +1100

gfarm (2.6.7+dfsg-1) unstable; urgency=medium

  * New upstream release [November 2015].

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 28 Nov 2015 19:54:47 +1100

gfarm (2.6.6+dfsg-1) unstable; urgency=medium

  * New upstream release [August 2015].
  * Updated URL of the upstream home page.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 30 Aug 2015 12:32:37 +1000

gfarm (2.6.5.1+dfsg-1) unstable; urgency=medium

  * New upstream release [July 2015].
  * libgfarm-dev: do not provide static library files (*.a) any more.

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 08 Jul 2015 20:22:46 +1000

gfarm (2.6.5+dfsg-1) unstable; urgency=medium

  * New upstream release [June 2015].
  * Avoid collision between generated and package-provided services:
    - dropped "debian.patch" (custom .service files should stay under "/etc").
    - postinst: do not remove .service files from "/lib/systemd/system",
      trust dh_systemd helper to do the clean-up.
  * gfarm-client: install utilities `gfsudo`, `gfpcopy` and `gfprep`.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 25 Jun 2015 18:16:18 +1000

gfarm (2.6.4.1+dfsg-1) unstable; urgency=medium

  * New upstream release [April 2015].
  * gfarm-client: install `/usr/bin/gfln`.

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 06 May 2015 00:45:00 +1000

gfarm (2.6.4+dfsg-1) unstable; urgency=low

  * New upstream release [April 2015].

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 26 Apr 2015 20:06:11 +1000

gfarm (2.6.3+dfsg-1) unstable; urgency=low

  * New upstream release [April 2015].
  * Upload to unstable.

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 15 Apr 2015 03:58:38 +1000

gfarm (2.6.2+dfsg-1) experimental; urgency=low

  * New upstream release [March 2015].
  * Install new man pages.
  * Install "unconfig" script templates.
  * watch: introduced "repacksuffix=+dfsg".

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 27 Mar 2015 21:11:18 +1100

gfarm (2.6.1+dfsg-1) experimental; urgency=low

  * New upstream release [March 2015].
  * Install new [config-gfarm.8, config-gfarm-update.8] man pages.
  * gfmd: promote "postgresql | slapd" to Recommends.
  * Patchworks:
    + New "pg_ctl.patch" to detect PostgreSQL version without "pg_config".
    - Removed [06_host_os_nickname, build-man, hardening, spelling, utf-8].

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 13 Mar 2015 10:39:31 +1100

gfarm (2.6.0+dfsg-2) experimental; urgency=low

  * Corrected symbols' architecture.
  * dh-systemd.
  * gfmd, gfsd: added "-v" option to service files.
    to log verbose messages on authentication.

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 06 Jan 2015 05:24:33 +1100

gfarm (2.6.0+dfsg-1) experimental; urgency=low

  * New upstream release [December 2014].
  * New maintainer (Closes: #715446); removed Uploaders.
  * Added "debian/gbp.conf".
  * Added Vcs links to new repository at collab-maint.
  * Removed obsolete "DM-Upload-Allowed".
  * Standards-Version: 3.9.6.
  * Debhelper & compat to version 9.
  * Use dh-autoreconf (Closes: #727868).
  * "--as-needed" to minimise needless linking.
  * Cleanup; removed obsolete README.Debian.
  * Install upstream changelog as such.
  * Don't install upstream build instructions.
  * Ignore changes in "makes/config.mk".
  * Simplified installation of man pages.
  * Build with all hardening.
  * Build man pages.
  * Re-build JAR files from "build-indep" target.
  * gfarm-client:
    + install more utilities and man pages.
    + added "gfarm2fs" to Recommends.
  * gfmd:
    + remove RPATH using "chrpath".
    + Suggests postgresql OR slapd, not both.
  * gfmd, gfsd:
    + added "gfarm-client" to Recommends.
    + optimised list of installed files.
    + install systemd service templates used by "config-gfarm".
    + install systemd services.
    + prevent automatic service start-up (dh_installinit --no-start).
    + added LSB description to init files.
    + new postrm handlers to remove systemd services or purge.
    + new postinst handlers to create "_gfarmfs" user and make
      "/var/lib/gfarmfs" directory; Depends: +adduser.
  * Multi-Arch for -doc and "libgfarm1" packages.
  * libgfarm:
    + install symbols.
    - don't install .la file.
  * watch: minor update; dfsg mangle.
  * copyright:
    + converted to copyright-format-1.0.
    + added Files-Excluded section to remove "hpux/getmemsize.c"
      (taken from not-for-sale HPUX FAQ) and pre-built JAR files.
    + Expat a.k.a. MIT license for Debian packaging.
  * Patchworks:
    - 00_libraries.patch
    - 09_change-shell.patch
    - debian-changes-2.4.1-1
    - 04_MAXPATHLEN.patch
    - 05_PATH_MAX.patch
    - 07_MAXHOSTNAMELEN.patch
    + build-hurd.patch (consolidated Hurd "MAX" definitions)
    + build-man.patch (correct man pages build process)
    + clean.patch (cleanup assist)
    + debian.patch (Debian path customisations)
    + defaults.patch (enable PostgreSQL "Data page checksums")
    + hardening.patch (use CPPFLAGS)
    + spelling.patch (minor spelling corrections)
    + utf8.patch (use UTF8 encoding to fix JAR re-build)
  * Build-Depends:
    + dh-autoreconf
    + chrpath
    + docbook-xml
    + docbook-xsl
    + xsltproc
    - postgresql
    - autoconf
    - libtool
  * Build-Depends-Indep:
    + ant
    + default-jdk

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 05 Jan 2015 20:42:29 +1100

gfarm (2.4.1-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Patch by Ilya Barygin <barygin@gmail.com>, set SHELL to /bin/bash as /bin/sh
    does not support +=. (Closes: #621926).

 -- Konstantinos Margaritis <markos@debian.org>  Tue, 31 Jan 2012 19:01:26 +0000

gfarm (2.4.1-1) unstable; urgency=low

  * New upstream release.

 -- NIIBE Yutaka <gniibe@fsij.org>  Mon, 14 Feb 2011 17:25:35 +0900

gfarm (2.4.0-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/00_libraries.patch: Refreshed.
  * debian/gfmd.manpages_{en,ja}: Remove uninstalled entries.
  * debian/gfsd.manpages_{en,ja}: Remove uninstalled entries.
  * debian/gfarm-client.manpages_{en,ja}: Remove uninstalled entries.
  * debian/control (Standards-Version): Bump to 3.9.1.
  * debian/libgfarm-dev.install: Don't install .la file (follow policy 3.9.1).

 -- NIIBE Yutaka <gniibe@fsij.org>  Wed, 24 Nov 2010 11:06:15 +0900

gfarm (2.3.2-1) unstable; urgency=low

  * New upstream release (Closes: #581101).
  * debian/patches/00_libraries.patch: Refreshed.
  * debian/patches/07_MAXHOSTNAMELEN.patch: Refreshed.
  * debian/patches/05_PATH_MAX.patch: Refreshed.
  * debian/gfarm-client.install (gfsched): Added.

 -- NIIBE Yutaka <gniibe@fsij.org>  Fri, 02 Jul 2010 10:00:53 +0900

gfarm (2.3.1-1) unstable; urgency=low

  * debian/control (Description, Suggests): Description improvement
    adding Suggests.  Thanks to Justin B Rye <jbr@edlug.org.uk>
    (Closes: #573516).
  * New upstream release.
  * debian/rules: Support manpages_en/_ja.
  * debian/libgfarm-dev.manpages_ja: New file.
  * debian/libgfarm-dev.manpages_en: Renamed (was: no _en).
  * debian/{gfmd.manpages_ja,gfsd.manpages_ja}: New files.
  * debian/{gfmd.manpages_en,gfsd.manpages_en}: Renamed (was: no _en).
  * debian/{gfarm-client.manpages_en,gfarm-client.manpages_ja}: Updated.
  * debian/{gfarm-doc.docs,gfarm-client.install,gfmd.install}: Updated.
  * debian/control (Package): New package libgfarm1 to match soname.
  * debian/patches/07_MAXHOSTNAMELEN.patch: New file to workaround on Hurd.
  * debian/README.source: Removed.

 -- NIIBE Yutaka <gniibe@fsij.org>  Tue, 08 Jun 2010 11:55:28 +0900

gfarm (2.3.0-5) unstable; urgency=low

  * debian/patches/06_host_os_nickname.patch: Updated.
  * debian/watch: Fix to exclude snapshot tar balls.

 -- NIIBE Yutaka <gniibe@fsij.org>  Tue, 11 May 2010 09:49:00 +0900

gfarm (2.3.0-4) unstable; urgency=low

  * debian/patches/{04_MAXPATHLEN.patch,05_PATH_MAX.patch,
    06_host_os_nickname.patch}: New files.

 -- NIIBE Yutaka <gniibe@fsij.org>  Mon, 10 May 2010 14:11:22 +0900

gfarm (2.3.0-3) unstable; urgency=low

  * debian/gfarm-client.manpages_ja: New file.
  * debian/rules: Invoke dh_installman with --language=ja.
  * debian/rules: Kill the bash-ism.
  * debian/rules: Don't use libtoolize but just copy ltmain.sh.
  * debian/patches/{00_libraries,01_noneed_yacc_lex,02_dont_go_doc,
    03_libtool_clean_does_not_work_well}.patch: Divided up.

 -- NIIBE Yutaka <gniibe@fsij.org>  Thu, 18 Mar 2010 09:16:05 +0900

gfarm (2.3.0-2) unstable; urgency=low

  * debian/patches/00_libraries.patch: Name it.
  * debian/rules: Build with _GNU_SOURCE for FTBFS on gfevent.c.

 -- NIIBE Yutaka <gniibe@fsij.org>  Wed, 17 Mar 2010 21:19:06 +0900

gfarm (2.3.0-1) unstable; urgency=low

  * Initial release (Closes: #568981).

 -- NIIBE Yutaka <gniibe@fsij.org>  Tue, 16 Feb 2010 15:30:53 +0900
